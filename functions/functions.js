function bmi_calculator() {
    let h = document.getElementById("heightInput").value;
    let h_selected = document.getElementById("heightTypeSelector").value;
    h *= h_selected;
    let w = document.getElementById("weightInput").value;
    let w_selected = document.getElementById("weightTypeSelector").value;
    w *= w_selected;
    let result = w / (h ** 2);
    let res_div = document.getElementById("result");
    res_div.removeChild(res_div.firstChild);
    let res_lbl = document.createElement("P");
    res_lbl.innerHTML = "Your BMI rate is: " + String(result);
    document.getElementById("result").appendChild(res_lbl);
}

function first_deg_eq_calculator() {
    let s = document.getElementById("slopeInput").value;
    let ic = document.getElementById("interceptInput").value;
    let result = "x * " + s + " + " + ic;
    let point = [-1 * ic / s, 0];
    plot_options.data[1].points.push(point);
    plot_options.data[0].fn = result;
    plot_options.data[0].derivative.fn = "x * " + s;
    functionPlot(plot_options);
    let res_div = document.getElementById("result");
    res_div.removeChild(res_div.firstChild);
    let res_lbl = document.createElement("P");
    res_lbl.innerHTML = "The Equation is : " + String(result) + " = 0 <br> The Answer is : x = " + point[0];
    document.getElementById("result").appendChild(res_lbl);
}

function second_deg_eq_calculator() {
    let a = document.getElementById("secDegInput").value;
    let b = document.getElementById("firstDegInput").value;
    let c = document.getElementById("interceptInput").value;
    let delta = (b * b) - (4 * a * c);
    let equation = "";
    let ap = a;
    if (a == 1) ap = "";
    if (a != 0)
        equation += ap + "x<sup>2</sup> ";
    let bp = b;
    if (b == 1) bp = "";
    if (b < 0) equation += "- " + (-1 * bp) + "x ";
    else if (b > 0) equation += "+ " + bp + "x ";
    if (c < 0) equation += "- " + (-1 * c) + " = 0";
    else if (c > 0) equation += "+ " + c + " = 0";
    let result = equation;
    let point;
    plot_options.data[1].points.pop();
    if (delta < 0) {
        result = "Error"
    } else if (delta > 0) {
        let x1 = (-b + Math.sqrt(delta)) / (2 * a);
        let x2 = (-b - Math.sqrt(delta)) / (2 * a);
        point = [[x1, 0], [x2, 0]];
        result += "<br>First answer is (x1 = " + x1 + ")<br>Second answer is (x2 = " + x2 + ")";
        plot_options.data[1].points = point;
        plot_options.data[0].fn = "(" + a + "*(x^2)) + " + b + "*x + " + c;
        functionPlot(plot_options);
    } else {
        let x = -b / (2 * a);
        point = [[x, 0]];
        result += "<br>The only answer is " + x;
        plot_options.data[1].points = point;
        plot_options.data[0].fn = "(" + a + "*(x^2)) + " + b + "*x + " + c;
        functionPlot(plot_options);
    }
    let res_div = document.getElementById("result");
    res_div.removeChild(res_div.firstChild);
    let res_lbl = document.createElement("P");
    res_lbl.innerHTML = result;
    document.getElementById("result").appendChild(res_lbl);
}

function linear_eq_calculator() {
    let s = document.getElementById("slopeInput").value;
    let ic = document.getElementById("interceptInput").value;
    let result = "x * " + s + " + " + ic;
    plot_options.data[0].fn = result;
    plot_options.data[0].derivative.fn = "x * " + s;
    functionPlot(plot_options);
    let res_div = document.getElementById("result");
    res_div.removeChild(res_div.firstChild);
    let res_lbl = document.createElement("P");
    res_lbl.innerHTML = "Your Equation is: y = " + String(result);
    document.getElementById("result").appendChild(res_lbl);
}

function ohms_law_calculator() {
    let inputs_array = [];
    let flag = 0;
    let current = document.getElementById("currentInput").value;
    inputs_array.push(current);
    let curr_sel = document.getElementById("currentTypeSelector").value;
    current *= curr_sel;
    let voltage = document.getElementById("voltageInput").value;
    inputs_array.push(voltage);
    let vol_sel = document.getElementById("voltageTypeSelector").value;
    voltage *= vol_sel;
    let resistance = document.getElementById("resistanceInput").value;
    inputs_array.push(resistance);
    let res_sel = document.getElementById("resistanceTypeSelector").value;
    resistance *= res_sel;
    let power = document.getElementById("powerInput").value;
    inputs_array.push(power);
    let pow_sel = document.getElementById("powerTypeSelector").value;
    power *= pow_sel;
    for (let i = 0; i < inputs_array.length; i++) {
        if (inputs_array[i] === "") {
            flag++;
        }
    }
    if (flag !== 2) {
        alert("You should enter two types of inputs");
    } else {
        let result, r, p;
        let res_div = document.getElementById("result");
        res_div.removeChild(res_div.firstChild);
        if (resistance === 0 && power === 0) {
            r = "Resistance is : " + voltage / current;
            p = "Power is : " + voltage * current;
            result = r + "<br>" + p;
        } else if (voltage === 0 && power === 0) {
            r = "Voltage is : " + resistance * current;
            p = "Power is : " + resistance * current * current;
            result = r + "<br>" + p;

        } else if (current === 0 && power === 0) {
            r = "Current is : " + voltage / resistance;
            p = "Power is : " + ((voltage * voltage) / resistance);
            result = r + "<br>" + p;

        } else if (voltage === 0 && resistance === 0) {
            r = "Voltage is : " + power / current;
            p = "Resistance is : " + power / (current * current);
            result = r + "<br>" + p;

        } else if (current === 0 && resistance === 0) {
            r = "Current is : " + power / voltage;
            p = "Resistance is : " + voltage * voltage / power;
            result = r + "<br>" + p;

        } else if (current === 0 && voltage === 0) {
            r = "Current is : " + Math.sqrt(power / resistance);
            p = "Voltage is : " + Math.sqrt(power * resistance);
            result = r + "<br>" + p;
        }
        let res1_lbl = document.createElement("P");
        res1_lbl.innerHTML = result;
        document.getElementById("result").appendChild(res1_lbl);
    }
}

function equilateral_triangle_calculator() {
    let a_in = document.getElementById("a_in");
    let h_in = document.getElementById("h_in");
    let area_in = document.getElementById("area_in");
    let perimeter_in = document.getElementById("perimeter_in");
    let circumcircle_in = document.getElementById("circumcircle_in");
    let incircle_in = document.getElementById("incircle_in");
    const sixty = Math.PI * 2 / 3;
    let a = null;
    if (a_in.value != 0) {
        a = a_in.value;
    } else if (h_in.value != 0) {
        const h = h_in.value;
        a = h / Math.sin(Math.PI * 2 / 3);
    } else if (area_in.value != 0) {
        const area = area_in.value;
        a = Math.sqrt(area * 2 / Math.sin(sixty));
    } else if (perimeter_in.value != 0) {
        const p = perimeter_in.value;
        a = p / 3;
    } else if (circumcircle_in.value != 0) {
        const c = circumcircle_in.value;
        a = c * 3 / Math.sqrt(3);
    } else if (incircle_in.value != 0) {
        const i = incircle_in.value;
        a = i * 6 / Math.sqrt(3);
    }
    a_in.value = a;
    h_in.value = a * Math.sin(sixty);
    area_in.value = a * h_in.value / 2;
    perimeter_in.value = a * 3;
    circumcircle_in.value = h_in.value * 2 / 3;
    incircle_in.value = h_in.value / 3;
}

function right_triangle_calculator() {
    let a_in = document.getElementById("a_in");
    let b_in = document.getElementById("b_in");
    let c_in = document.getElementById("c_in");
    let area_in = document.getElementById("area_in");
    let perimeter_in = document.getElementById("perimeter_in");
    let a = null;
    let b = null;
    if (a_in.value != 0 && b_in.value != 0) {
        a = a_in.value;
        b = b_in.value;
    } else if (c_in.value != 0 && (a_in.value != 0 || b_in.value != 0)) {
        const c = c_in.value;
        if (a_in.value != 0) {
            a = a_in.value;
            b = Math.sqrt(c ** 2 - a ** 2);
        } else if (b_in.value != 0) {
            b = b_in.value;
            a = Math.sqrt(c ** 2 - b ** 2);
        }
    } else if (area_in.value != 0 && (a_in.value != 0 || b_in.value != 0)) {
        const area = area_in.value;
        if (a_in.value != 0) {
            a = a_in.value;
            b = area * 2 / a;
        } else if (b_in.value != 0) {
            b = b_in.value;
            a = area * 2 / b;
        }
    }
    a_in.value = a;
    b_in.value = b;
    c_in.value = Math.sqrt(a ** 2 + b ** 2);
    area_in.value = a * b / 2;
    perimeter_in.value = a + b + c_in.value;
}

function isosceles_triangle_calculator() {
    let a_in = document.getElementById("a_in");
    let b_in = document.getElementById("b_in");
    let hb_in = document.getElementById("hb_in");
    let ha_in = document.getElementById("ha_in");
    let beta_in = document.getElementById("beta_in");
    let alpha_in = document.getElementById("alpha_in");
    let area_in = document.getElementById("area_in");
    let perimeter_in = document.getElementById("perimeter_in");
    let a = null;
    let b = null;
    if (a_in.value != 0 && b_in.value != 0) {
        a = a_in.value;
        b = b_in.value;
    } else if (a_in.value != 0 && ha_in.value != 0) {
        a = a_in.value;
        const ha = ha_in.value;
        b = Math.sqrt(4 * (a ** 2 - ha ** 2));
    } else if (b_in.value != 0 && hb_in.value != 0) {
        b = b_in.value;
        const hb = hb_in.value;
        a = Math.sqrt((b / 2) ** 2 + hb ** 2);
    } else if (a_in.value != 0 && (alpha_in.value != 0 || beta_in.value != 0)) {
        a = a_in.value != 0;
        let angle;
        if (alpha_in.value != 0) {
            angle = alpha_in.value;
        } else {
            angle = (2 * Math.PI - beta_in.value) / 2;
        }
        b = Math.cos(angle) * a * 2;
    } else if (b_in.value != 0 && (alpha_in.value != 0 || beta_in.value != 0)) {
        b = b_in.value != 0;
        let angle;
        if (alpha_in.value != 0) {
            angle = alpha_in.value;
        } else {
            angle = (2 * Math.PI - beta_in.value) / 2;
        }
        a = b / (2 * Math.cos(angle));
    } else if (area_in.value != 0 && ha_in.value != 0) {
        a = 2 * area_in.value / ha_in.value;
        b = Math.sqrt(4 * (a ** 2 - ha_in.value ** 2));
    } else if (area_in.value != 0 && hb_in.value != 0) {
        b = 2 * area_in.value / hb_in.value;
        a = Math.sqrt((b / 2) ** 2 + hb_in.value ** 2);
    } else if (area_in.value != 0 && (alpha_in.value != 0 || beta_in.value != 0)) {
        let angle;
        if (alpha_in.value != 0) {
            angle = alpha_in.value;
        } else {
            angle = (2 * Math.PI - beta_in.value) / 2;
        }
        b = Math.sqrt(area_in.value * 6 / Math.sqrt(3));
        a = b / (2 * Math.cos(angle));
    } else if (perimeter_in.value != (alpha_in.value != 0 || beta_in.value != 0)) {
        let angle;
        if (alpha_in.value != 0) {
            angle = alpha_in.value;
        } else {
            angle = (2 * Math.PI - beta_in.value) / 2;
        }
        a = perimeter_in.value / (2 + Math.cos(angle));
        b = Math.cos(angle) * a * 2;
    }
    a_in.value = a;
    b_in.value = b;
    area_in.value = (1 / 4) * b * Math.sqrt(4 * (a ** 2) - (b ** 2));
    ha_in.value = (area_in.value * 2) / b;
    hb_in.value = (area_in.value * 2) / a;
    alpha_in.value = Math.acos(b / (2 * a));
    beta_in.value = 2 * Math.asin(b / (2 * a));
    perimeter_in.value = a + a + b;
}

function heron_triangle_calculator() {
    let a_in = document.getElementById("a_in");
    let b_in = document.getElementById("b_in");
    let c_in = document.getElementById("c_in");
    let area_in = document.getElementById("area_in");
    let perimeter_in = document.getElementById("perimeter_in");
    let a, b, c;
    if (a_in.value != 0 && b_in.value != 0 && c_in.value != 0) {
        a = parseFloat(a_in.value);
        b = parseFloat(b_in.value);
        c = parseFloat(c_in.value);
    } else if (perimeter_in.value != 0) {
        if (a_in.value != 0 && b_in.value != 0) {
            a = parseFloat(a_in.value);
            b = parseFloat(b_in.value);
            c = perimeter_in.value - a - b;
        } else if (a_in.value != 0 && c_in.value != 0) {
            a = parseFloat(a_in.value);
            c = parseFloat(c_in.value);
            b = perimeter_in.value - a - c;
        } else if (b_in.value != 0 && c_in.value != 0) {
            b = parseFloat(b_in.value);
            c = parseFloat(c_in.value);
            a = perimeter_in.value - b - c;
        }
    } else if (area_in.value != 0) {
        if (a_in.value != 0 && b_in.value != 0) {
            a = parseFloat(a_in.value);
            b = parseFloat(b_in.value);
            const area = area_in.value;
            c = Math.sqrt((b ** 2) + (a ** 2) + ((-2) * b * a) * Math.cos(Math.asin((area * 2) / (b * a))));
            c_in.value = c;
            perimeter_in.value = a + b + c;
        } else if (a_in.value != 0 && c_in.value != 0) {
            a = parseFloat(a_in.value);
            c = parseFloat(c_in.value);
            const area = area_in.value;
            b = Math.sqrt((c ** 2) + (a ** 2) + ((-2) * c * a) * Math.cos(Math.asin((area * 2) / (c * a))));
            b_in.value = b;
            perimeter_in.value = a + b + c;
        } else if (b_in.value != 0 && c_in.value != 0) {
            b = parseFloat(b_in.value);
            c = parseFloat(c_in.value);
            const area = area_in.value;
            a = Math.sqrt((b ** 2) + (c ** 2) + ((-2) * b * c) * Math.cos(Math.asin((area * 2) / (b * c))));
            a_in.value = a;
            perimeter_in.value = a + b + c;
        }
        return;
    }
    const p = (parseFloat(a) + parseFloat(b) + parseFloat(c)) / 2;
    area_in.value = Math.sqrt(p * (p - a) * (p - b) * (p - c));
    perimeter_in.value = p * 2;
}

function cross_product_calculator() {
    let a_x_in = document.getElementById("a_x_in");
    let a_y_in = document.getElementById("a_y_in");
    let a_z_in = document.getElementById("a_z_in");
    let b_x_in = document.getElementById("b_x_in");
    let b_y_in = document.getElementById("b_y_in");
    let b_z_in = document.getElementById("b_z_in");
    let c_x_in = document.getElementById("c_x_in");
    let c_y_in = document.getElementById("c_y_in");
    let c_z_in = document.getElementById("c_z_in");

    if (a_x_in.value != 0 && a_y_in.value != 0 && a_z_in.value != 0 && b_x_in.value != 0 && b_y_in.value != 0 && b_z_in.value != 0) {
        const a_x = a_x_in.value;
        const a_y = a_y_in.value;
        const a_z = a_z_in.value;
        const b_x = b_x_in.value;
        const b_y = b_y_in.value;
        const b_z = b_z_in.value;
        c_x_in.value = a_y * b_z - a_z * b_y;
        c_y_in.value = a_z * b_x - a_x * b_z;
        c_z_in.value = a_x * b_y - a_y * b_x;
    }
}

function dot_product_calculator() {
    let a_x_in = document.getElementById("a_x_in");
    let a_y_in = document.getElementById("a_y_in");
    let a_z_in = document.getElementById("a_z_in");
    let b_x_in = document.getElementById("b_x_in");
    let b_y_in = document.getElementById("b_y_in");
    let b_z_in = document.getElementById("b_z_in");
    let c_in = document.getElementById("c_in");

    if (a_x_in.value != 0 && a_y_in.value != 0 && a_z_in.value != 0 && b_x_in.value != 0 && b_y_in.value != 0 && b_z_in.value != 0) {
        const a_x = a_x_in.value;
        const a_y = a_y_in.value;
        const a_z = a_z_in.value;
        const b_x = b_x_in.value;
        const b_y = b_y_in.value;
        const b_z = b_z_in.value;
        c_in.value = a_x * b_x + a_y * b_y + a_z * b_z;
    }
}

function perpendicular_line_calculator() {
    let fist_eq_m_in = document.getElementById("fist_eq_m_in");
    let first_eq_r_in = document.getElementById("first_eq_r_in");
    let sec_px_in = document.getElementById("sec_px_in");
    let sec_py_in = document.getElementById("sec_py_in");
    let sec_eq_a_in = document.getElementById("sec_eq_a_in");
    let sec_eq_b_in = document.getElementById("sec_eq_b_in");
    let inter_px_in = document.getElementById("inter_px_in");
    let inter_py_in = document.getElementById("inter_py_in");
    let fist_eq_m = document.getElementById("fist_eq_m_in");
    let first_eq_r = document.getElementById("first_eq_r_in");
    let sec_px = document.getElementById("sec_px_in").value;
    let sec_py = document.getElementById("sec_py_in").value;
    let sec_eq_a = document.getElementById("sec_eq_a_in").value;
    let sec_eq_b = document.getElementById("sec_eq_b_in").value;
    let inter_px = document.getElementById("inter_px_in").value;
    let inter_py = document.getElementById("inter_py_in").value;

    if (fist_eq_m != 0 && first_eq_r != 0) {
        if (sec_px != 0 && sec_py != 0) {
            console.log("here");
            sec_eq_a_in.value = -1 / fist_eq_m_in.value;
            sec_eq_b_in.value = sec_py_in.value - sec_eq_a_in.value * sec_px_in.value;
            inter_px_in.value = -(first_eq_r_in.value - sec_eq_b_in.value) / (fist_eq_m_in.value - sec_eq_a_in.value);
            inter_py_in.value = inter_px_in.value * fist_eq_m_in.value + first_eq_r_in.value;
        } else if (sec_eq_a != 0 && sec_eq_b != 0) {
            inter_px_in.value = -(first_eq_r_in.value - sec_eq_b_in.value) / (fist_eq_m_in.value - sec_eq_a_in.value);
            inter_py_in.value = inter_px_in.value * fist_eq_m_in.value + first_eq_r_in.value;
        } else if (inter_px != 0 && inter_py != 0) {
            sec_eq_a_in.value = -1 / fist_eq_m_in.value;
            sec_eq_b_in.value = inter_py_in.value - sec_eq_a_in.value * inter_px_in.value;
        }
    }
}

function cylindrical_calculator() {
    let a_x_in = document.getElementById("a_x_in");
    let a_y_in = document.getElementById("a_y_in");
    let a_z_in = document.getElementById("a_z_in");
    let b_x_in = document.getElementById("b_x_in");
    let b_y_in = document.getElementById("b_y_in");
    let b_z_in = document.getElementById("b_z_in");

    if (a_x_in.value != 0 && a_y_in.value != 0 && a_z_in.value != 0) {
        const a_x = a_x_in.value;
        const a_y = a_y_in.value;
        const a_z = a_z_in.value;
        b_x_in.value = Math.sqrt((a_x ** 2) + (a_y ** 2));
        b_y_in.value = 360 * Math.atan(a_y / a_x) / (Math.PI * 2);
        b_z_in.value = a_z;
    } else if (b_x_in.value != 0 && b_y_in.value != 0 && b_z_in.value != 0) {
        const b_x = b_x_in.value;
        const b_y = b_y_in.value;
        const b_z = b_z_in.value;
        a_x_in.value = Math.cos(b_y) * b_x;
        a_y_in.value = Math.sin(b_y) * b_x;
        a_z_in.value = b_z;
    }
}

function shoe_size_calculator() {
    let l_in = document.getElementById("l_in");
    let usw_in = document.getElementById("usw_in");
    let usm_in = document.getElementById("usm_in");
    let au_in = document.getElementById("au_in");
    let eu_in = document.getElementById("eu_in");
    let jp_in = document.getElementById("jp_in");
    let kr_in = document.getElementById("kr_in");

    let inches;
    if (l_in.value != 0) {
        inches = l_in.value / 25.4;
    } else if (usw_in.value != 0) {
        inches = (usw_in.value + 22) / 3;
    } else if (usm_in.value != 0) {
        inches = (usm_in.value + 21) / 3;
    } else if (au_in.value != 0) {
        inches = (au_in.value + 23) / 3;
    } else if (eu_in.value != 0) {
        inches = ((eu_in.value - 2) / 1.27 + 23) / 3;
    } else if (jp_in.value != 0) {
        inches = jp_in * 10 / 25.4;
    } else if (kr_in.value != 0) {
        inches = kr_in / 25.4;
    }
    usw_in.innerHTML = inches * 3 - 21;
    usm_in.innerHTML = inches * 3 - 22;
    au_in.innerHTML = inches * 3 - 23;
    eu_in.innerHTML = (inches * 3 * 1.27) + 2;
    jp_in.innerHTML = inches * 25.4 / 10;
    kr_in.innerHTML = inches * 25.4;
    l_in.innerHTML = inches * 25.4;
}




