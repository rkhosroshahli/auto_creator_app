let resultCalculator = null;
let plot_options = null;
let input_id, result_id, display_id;
let curr_json;

let inputElements = [];

function findElement(query) {
    return document.querySelector(query);
}

function inputAreaCreator(inputs_header = 'Inputs', id = 'input', className = 'col-sm-3') {
    var col_div = document.createElement("div");
    col_div.className = className;
    findElement('#row_div').appendChild(col_div);
    var card_div = document.createElement("div");
    card_div.className = 'card bg-light';
    col_div.appendChild(card_div);
    var header_div = document.createElement("div");
    header_div.className = 'card-header';
    header_div.innerHTML = inputs_header;
    card_div.appendChild(header_div);
    var body_div = document.createElement("div");
    body_div.className = 'card-body';
    body_div.id = id;
    card_div.appendChild(body_div);

}

function displayAreaCreator(inputs_header, id, className = 'col-sm-6') {
    var col_div = document.createElement("div");
    col_div.className = className;
    findElement('#row_div').appendChild(col_div);
    var card_div = document.createElement("div");
    card_div.className = 'card bg-light';
    col_div.appendChild(card_div);
    var header_div = document.createElement("div");
    header_div.className = 'card-header';
    header_div.innerHTML = inputs_header;
    card_div.appendChild(header_div);
    var body_div = document.createElement("div");
    body_div.className = 'card-body text-center';
    body_div.id = id;
    card_div.appendChild(body_div);
}

function outputAreaCreator(inputs_header = 'output', id = 'output', className = 'col-sm-3') {
    var col_div = document.createElement("div");
    col_div.className = className;
    findElement('#row_div').appendChild(col_div);
    var card_div = document.createElement("div");
    card_div.className = 'card bg-light';
    col_div.appendChild(card_div);
    var header_div = document.createElement("div");
    header_div.className = 'card-header';
    header_div.innerHTML = inputs_header;
    card_div.appendChild(header_div);
    var body_div = document.createElement("div");
    body_div.className = 'card-body';
    body_div.id = id;
    card_div.appendChild(body_div);
}

function areaCreator(inputs_header, id, col_class, body_class = '') {
    var col_div = document.createElement("div");
    col_div.className = col_class;
    findElement('#row_div').appendChild(col_div);
    var card_div = document.createElement("div");
    card_div.className = 'card bg-light';
    col_div.appendChild(card_div);
    var header_div = document.createElement("div");
    header_div.className = 'card-header';
    header_div.innerHTML = inputs_header;
    card_div.appendChild(header_div);
    var body_div = document.createElement("div");
    body_div.className = 'card-body '.concat(body_class);
    body_div.id = id;
    card_div.appendChild(body_div);
}

function switcher(attr) {
    switch (attr.type) {
        case "text": {
            return textElementCreator(attr);
        }
        case "range": {
            return rangeInputCreator(attr);
        }
        case "button": {
            return buttonElementCreator(attr);
        }
        case "select": {
            return selectElementCreator(attr);
        }
        case "radio": {
            return radiosElementCreator(attr);
        }
        case "checkbox":
            return radiosElementCreator(attr);
        default:
            console.log("There is problem");
    }
}

function jsonHandler(json) {
    curr_json = json;
    const inputEl = json.areas.input;
    input_id = inputEl.id;
    areaCreator(inputs_header = inputEl.inputs_header, id = inputEl.id, col_class = inputEl.col_class);
    let form = document.createElement("FORM");
    for (const [key, attr] of Object.entries(json.attributes)) {
        let element = switcher(attr);
        form.appendChild(element);
    }
    findElement('#' + inputEl.id).appendChild(form);
    link_input_monitors();
    $('input[type=range]').eq(0).change();
    if (json.areas.display) {
        const displayEl = json.areas.display;
        areaCreator(inputs_header = displayEl.inputs_header, id = displayEl.id, col_class = displayEl.col_class, displayEl.body_class);
        let chart = chartElementCreator(json);
        findElement('#' + displayEl.id).appendChild(chart);
    }
}

$(document).ready(function () {
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    let url = urlParams.get('json');
    var jsonFile = "./inputs/" + url;
    fetch(jsonFile)
        .then(response => response.json())
        .then(json => jsonFile = json)
        .then(() => jsonHandler(jsonFile));
});

function link_input_monitors() {
    $('.input-monitor').each(function (index) {
        let e = $(this);
        let ds = e.attr('data-source');
        if (ds !== '') {
            var d = $('#' + ds);
            d.change(function () {
                e.html(d.val());
            });
            d.change();
        }
    });
}

function chartElementCreator(json) {
    if (json.charts.image) {
        let chart = document.createElement("IMG");
        chart.setAttribute('src', json.charts.image.url);
        chart.id = json.charts.image.id;
        display_id = chart.id;
        chart.style = "max-width: 100%;max-height: 100%;";
        return chart;
    }
    const props = json.charts.plot;
    let chart = document.createElement("DIV");
    chart.id = json.charts.plot.id;
    display_id = chart.id;
    chart.style = "max-width: 100%;max-height: 100%;";
    plot_options = {
        target: chart,
        xAxis: {domain: props.xAxis},
        yAxis: {domain: props.yAxis},
        grid: !!props.grid,
        data: props.data
    };
    console.log(plot_options)
    functionPlot(plot_options);
    return chart;
}

function textElementCreator(attr) {
    let div = document.createElement("DIV");
    div.className = "form-group";
    if (attr.label) {
        let label = document.createElement("P");
        const labelAttr = attr.label;
        label.innerHTML = labelAttr.text;
        if (labelAttr.isBold)
            label.className = "font-weight-bold";
        label.setAttribute('for', attr.id);
        div.appendChild(label);
    }
    let element = document.createElement(attr.tagName);
    element.className = attr.className;
    element.id = attr.id;
    element.setAttribute('placeholder', attr.placeholder);
    element.setAttribute('type', attr.type);
    if (attr.options) {
        let subDiv = document.createElement("DIV");
        subDiv.className = "form-row";
        let elmntDiv = document.createElement("DIV");
        elmntDiv.className = "col-sm-8";
        elmntDiv.appendChild(element);
        let optionDiv = document.createElement("DIV");
        optionDiv.className = "col-sm-4";
        let s = selectElementCreator(attr.options);
        optionDiv.appendChild(s);
        subDiv.appendChild(elmntDiv);
        subDiv.appendChild(optionDiv);
        div.appendChild(subDiv);
    } else {
        div.appendChild(element);
    }
    inputElements.push(element);
    return div;
}

function rangeInputCreator(attr) {
    let div = document.createElement("DIV");
    div.className = "form-group";
    let label = document.createElement("LABEL");
    const labelAttr = attr.label;
    label.innerHTML = labelAttr.text;
    if (labelAttr.isBold)
        label.className = "font-weight-bold";
    div.appendChild(label);
    let valueSpan = document.createElement("SPAN");
    valueSpan.className = "input-monitor";
    valueSpan.setAttribute('data-source', attr.id);
    div.appendChild(valueSpan);
    let element = document.createElement(attr.tagName);
    element.className = attr.className;
    element.id = attr.id;
    element.setAttribute('type', attr.type);
    element.setAttribute('min', attr.minVal);
    element.setAttribute('max', attr.maxVal);
    element.setAttribute('value', attr.value);
    element.setAttribute('step', attr.step);
    div.appendChild(element);
    inputElements.push(element);
    return div;
}


function buttonElementCreator(attr) {
    let element = document.createElement(attr.tagName);
    element.innerHTML = attr.innerHTML;
    element.className = attr.className;
    element.id = attr.id;
    resultCalculator = eval(attr.onclick);
    element.onclick = resultCalculator;
    element.setAttribute('type', attr.type);
    return element;
}

function selectElementCreator(attr) {
    let div = document.createElement("DIV");
    div.className = "form-group";
    if (attr.label) {
        let label = document.createElement("LABEL");
        const labelAttr = attr.label;
        label.innerHTML = labelAttr.text;
        if (labelAttr.isBold) {
            label.className = "font-weight-bold";
        }
        div.appendChild(label);
    }
    let element = document.createElement(attr.tagName);
    for (const op of attr.innerHTML) {
        let child = document.createElement("OPTION");
        child.innerHTML = op.text;
        child.setAttribute('value', op.value);
        element.appendChild(child);
    }
    element.className = attr.className;
    element.id = attr.id;
    div.appendChild(element);
    return div;
}

function radiosElementCreator(attr) {
    let fatherDiv = document.createElement("DIV");
    if (attr.mainLabel) {
        fatherDiv.className = "form-group";
        label = document.createElement("p");
        label.innerHTML = attr.mainLabel.text;
        if (attr.mainLabel.isBold)
            label.className = "font-weight-bold";
        fatherDiv.appendChild(label);
    }
    let mainDiv = document.createElement("DIV");
    if (attr.subLabel) {
        mainDiv.className = "form-group row";
        label = document.createElement("label");
        label.setAttribute('for', attr.name);
        label.className = "col-sm-8 col-form-label";
        label.innerHTML = attr.subLabel.text;
        if (attr.subLabel.isBold)
            label.className = "font-weight-bold";
        mainDiv.appendChild(label);
        mainDiv.appendChild(label);
        fatherDiv.appendChild(mainDiv);
    }
    var subDiv = document.createElement("DIV");
    subDiv.id = attr.name;
    subDiv.className = "col-sm-4"
    for (const [key, radio] of Object.entries(attr.subInputs)) {
        let inputDiv = document.createElement("DIV");
        inputDiv.className = "form-check";
        let radioElement = document.createElement("INPUT");
        radioElement.className = attr.className;
        radioElement.setAttribute('type', attr.type);
        if (attr.type === 'radio') {
            radioElement.setAttribute('name', attr.name);
        }
        radioElement.setAttribute('value', radio.value);
        radioElement.id = radio.id;
        inputDiv.appendChild(radioElement);
        let label = document.createElement("LABEL");
        label.innerHTML = radio.label.text;
        label.className = radio.label.className;
        label.setAttribute('for', radio.id);
        if (label.isBold) {
            label.className = "font-weight-bold";
        }
        inputDiv.appendChild(label);
        subDiv.appendChild(inputDiv);

    }
    if (attr.subLabel) mainDiv.appendChild(subDiv);
    else fatherDiv.appendChild(subDiv);
    return fatherDiv;
}

function clearContent() {
    for (let element of inputElements) {
        if (element.type === "range") {
            element.value = element.min;
        } else if (element.type === "text") {
            element.value = "";
        }
    }
}

function bmi_calculator() {
    let h = findElement("#heightInput").value;
    let h_selected = findElement("#heightTypeSelector").value;
    h *= h_selected;
    let w = findElement("#weightInput").value;
    let w_selected = findElement("#weightTypeSelector").value;
    w *= w_selected;
    let result = w / (h ** 2);
    let res_lbl = document.createElement("P");
    res_lbl.innerHTML = "Your BMI rate is: " + String(result);
    if (curr_json.areas.output && !findElement('#' + result_id)) {
        const resultEl = curr_json.areas.output;
        areaCreator(inputs_header = resultEl.inputs_header, id = resultEl.id, col_class = resultEl.col_class);
        findElement('#' + resultEl.id).innerHTML = '';
        findElement('#' + resultEl.id).appendChild(res_lbl);
    }
}

function first_deg_eq_calculator() {
    let s = findElement("#slopeInput").value;
    let ic = findElement("#interceptInput").value;
    let result = "x * " + s + " + " + ic;
    let point = [-1 * ic / s, 0];

    plot_options.data[1].points.push(point);
    plot_options.data[0].fn = result;
    plot_options.data[0].derivative.fn = "x * " + s;
    console.log(plot_options)
    functionPlot(plot_options);
    let res_lbl = document.createElement("P");
    res_lbl.innerHTML = "The Equation is : " + String(result) + " = 0 <br> The Answer is : x = " + point[0];
    if (curr_json.areas.output && !findElement('#' + result_id)) {
        const resultEl = curr_json.areas.output;
        areaCreator(inputs_header = resultEl.inputs_header, id = resultEl.id, col_class = resultEl.col_class);
        findElement('#' + resultEl.id).innerHTML = '';
        findElement('#' + resultEl.id).appendChild(res_lbl);
    }
}

function second_deg_eq_calculator() {
    let a = findElement("#secDegInput").value;
    let b = findElement("#firstDegInput").value;
    let c = findElement("#interceptInput").value;
    let delta = (b * b) - (4 * a * c);
    let equation = "";
    let ap = a;
    if (a == 1) ap = "";
    if (a != 0)
        equation += ap + "x<sup>2</sup> ";
    let bp = b;
    if (b == 1) bp = "";
    if (b < 0) equation += "- " + (-1 * bp) + "x ";
    else if (b > 0) equation += "+ " + bp + "x ";
    if (c < 0) equation += "- " + (-1 * c) + " = 0";
    else if (c > 0) equation += "+ " + c + " = 0";
    let result = equation;
    let point;
    plot_options.data[1].points.pop();
    if (delta < 0) {
        result = "Error"
    } else if (delta > 0) {
        let x1 = (-b + Math.sqrt(delta)) / (2 * a);
        let x2 = (-b - Math.sqrt(delta)) / (2 * a);
        point = [[x1, 0], [x2, 0]];
        result += "<br>First answer is (x1 = " + x1 + ")<br>Second answer is (x2 = " + x2 + ")";
        plot_options.data[1].points = point;
        plot_options.data[0].fn = "(" + a + "*(x^2)) + " + b + "*x + " + c;
        functionPlot(plot_options);
    } else {
        let x = -b / (2 * a);
        point = [[x, 0]];
        result += "<br>The only answer is " + x;
        plot_options.data[1].points = point;
        plot_options.data[0].fn = "(" + a + "*(x^2)) + " + b + "*x + " + c;
        functionPlot(plot_options);
    }
    let res_lbl = document.createElement("P");
    res_lbl.innerHTML = result;
    if (curr_json.areas.output && !findElement('#' + result_id)) {
        const resultEl = curr_json.areas.output;
        areaCreator(inputs_header = resultEl.inputs_header, id = resultEl.id, col_class = resultEl.col_class);
        findElement('#' + resultEl.id).innerHTML = '';
        findElement('#' + resultEl.id).appendChild(res_lbl);
    }
}

function linear_eq_calculator() {
    let s = findElement("#slopeInput").value;
    let ic = findElement("#interceptInput").value;
    let result = "x * " + s + " + " + ic;
    console.log(plot_options)
    plot_options.data[0].fn = result;
    plot_options.data[0].derivative.fn = "x * " + s;
    functionPlot(plot_options);
    let res_lbl = document.createElement("P");
    res_lbl.innerHTML = "Your Equation is: y = " + String(result);
    if (curr_json.areas.output && !findElement('#' + result_id)) {
        const resultEl = curr_json.areas.output;
        areaCreator(inputs_header = resultEl.inputs_header, id = resultEl.id, col_class = resultEl.col_class);
        findElement('#' + resultEl.id).innerHTML = '';
        findElement('#' + resultEl.id).appendChild(res_lbl);
    }
}

function ohms_law_calculator() {
    let inputs_array = [];
    let flag = 0;
    let current = findElement("#currentInput").value;
    inputs_array.push(current);
    let curr_sel = findElement("#currentTypeSelector").value;
    current *= curr_sel;
    let voltage = findElement("#voltageInput").value;
    inputs_array.push(voltage);
    let vol_sel = findElement("#voltageTypeSelector").value;
    voltage *= vol_sel;
    let resistance = findElement("#resistanceInput").value;
    inputs_array.push(resistance);
    let res_sel = findElement("#resistanceTypeSelector").value;
    resistance *= res_sel;
    let power = findElement("#powerInput").value;
    inputs_array.push(power);
    let pow_sel = findElement("#powerTypeSelector").value;
    power *= pow_sel;
    for (let i = 0; i < inputs_array.length; i++) {
        if (inputs_array[i] === "") {
            flag++;
        }
    }
    if (flag !== 2) {
        alert("You should enter two types of inputs");
    } else {
        let result, r, p;
        let res_div = document.getElementById("result");
        res_div.removeChild(res_div.firstChild);
        if (resistance === 0 && power === 0) {
            r = "Resistance is : " + voltage / current;
            p = "Power is : " + voltage * current;
            result = r + "<br>" + p;
        } else if (voltage === 0 && power === 0) {
            r = "Voltage is : " + resistance * current;
            p = "Power is : " + resistance * current * current;
            result = r + "<br>" + p;

        } else if (current === 0 && power === 0) {
            r = "Current is : " + voltage / resistance;
            p = "Power is : " + ((voltage * voltage) / resistance);
            result = r + "<br>" + p;

        } else if (voltage === 0 && resistance === 0) {
            r = "Voltage is : " + power / current;
            p = "Resistance is : " + power / (current * current);
            result = r + "<br>" + p;

        } else if (current === 0 && resistance === 0) {
            r = "Current is : " + power / voltage;
            p = "Resistance is : " + voltage * voltage / power;
            result = r + "<br>" + p;

        } else if (current === 0 && voltage === 0) {
            r = "Current is : " + Math.sqrt(power / resistance);
            p = "Voltage is : " + Math.sqrt(power * resistance);
            result = r + "<br>" + p;
        }
        let res1_lbl = document.createElement("P");
        res1_lbl.innerHTML = result;
        if (curr_json.areas.output && !findElement('#' + result_id)) {
            const resultEl = curr_json.areas.output;
            areaCreator(inputs_header = resultEl.inputs_header, id = resultEl.id, col_class = resultEl.col_class);
            findElement('#' + resultEl.id).innerHTML = '';
            findElement('#' + resultEl.id).appendChild(res1_lbl);
        }
    }
}

function equilateral_triangle_calculator() {
    let a_in_el = findElement("#a_in");
    let h_in_el = findElement("#h_in");
    let area_in_el = findElement("#area_in");
    let perimeter_in_el = findElement("#perimeter_in");
    let circumcircle_in_el = findElement("#circumcircle_in");
    let incircle_in_el = findElement("#incircle_in");
    const a_sel = findElement("#a_sel").value;
    const a_in = a_in_el.value * a_sel;
    const h_sel = findElement("#h_sel").value;
    const h_in = h_in_el.value * h_sel;
    const area_sel = findElement("#area_sel").value;
    const area_in = area_in_el.value * area_sel;
    const perimeter_sel = findElement("#perimeter_sel").value;
    const perimeter_in = perimeter_in_el.value * perimeter_sel;
    const circumcircle_sel = findElement("#circumcircle_sel").value;
    const circumcircle_in = circumcircle_in_el.value * circumcircle_sel;
    const incircle_sel = findElement("#incircle_sel").value;
    const incircle_in = incircle_in_el.value * incircle_sel;
    const sixty = Math.PI * 2 / 3;
    let a = null;
    if (a_in != 0) {
        a = a_in;
    } else if (h_in != 0) {
        const h = h_in;
        a = h / Math.sin(Math.PI * 2 / 3);
    } else if (area_in != 0) {
        const area = area_in;
        a = Math.sqrt(area * 2 / Math.sin(sixty));
    } else if (perimeter_in != 0) {
        const p = perimeter_in;
        a = p / 3;
    } else if (circumcircle_in != 0) {
        const c = circumcircle_in;
        a = c * 3 / Math.sqrt(3);
    } else if (incircle_in != 0) {
        const i = incircle_in;
        a = i * 6 / Math.sqrt(3);
    }
    a_in_el.value = a;
    h_in_el.value = a * Math.sin(sixty);
    area_in_el.value = a * h_in / 2;
    perimeter_in_el.value = a * 3;
    circumcircle_in_el.value = h_in * 2 / 3;
    incircle_in_el.value = h_in / 3;
}

function right_triangle_calculator() {
    let a_in_el = findElement("#a_in");
    let b_in_el = findElement("#b_in");
    let c_in_el = findElement("#c_in");
    let area_in_el = findElement("#area_in");
    let perimeter_in_el = findElement("#perimeter_in");
    const a_sel = findElement("#a_sel").value;
    const a_in = a_in_el.value * a_sel;
    const b_sel = findElement("#b_sel").value;
    const b_in = b_in_el.value * b_sel;
    const c_sel = findElement("#c_sel").value;
    const c_in = c_in_el.value * c_sel;
    const area_sel = findElement("#area_sel").value;
    const area_in = area_in_el.value * area_sel;
    const perimeter_sel = findElement("#perimeter_sel").value;
    const perimeter_in = perimeter_in_el.value * perimeter_sel;
    let a = null;
    let b = null;
    if (a_in != 0 && b_in != 0) {
        a = a_in;
        b = b_in;
    } else if (c_in != 0 && (a_in != 0 || b_in != 0)) {
        const c = c_in;
        if (a_in != 0) {
            a = a_in;
            b = Math.sqrt(c ** 2 - a ** 2);
        } else if (b_in != 0) {
            b = b_in.value;
            a = Math.sqrt(c ** 2 - b ** 2);
        }
    } else if (area_in != 0 && (a_in != 0 || b_in != 0)) {
        const area = area_in;
        if (a_in != 0) {
            a = a_in;
            b = area * 2 / a;
        } else if (b_in != 0) {
            b = b_in;
            a = area * 2 / b;
        }
    }
    a_in.value = a;
    b_in.value = b;
    c_in.value = Math.sqrt(a ** 2 + b ** 2);
    area_in.value = a * b / 2;
    perimeter_in.value = a + b + c_in;
}

function isosceles_triangle_calculator() {
    let a_in_el = findElement("#a_in");
    let b_in_el = findElement("#b_in");
    let hb_in_el = findElement("#hb_in");
    let ha_in_el = findElement("#ha_in");
    let beta_in_el = findElement("#beta_in");
    let alpha_in_el = findElement("#alpha_in");
    let area_in_el = findElement("#area_in");
    let perimeter_in_el = findElement("#perimeter_in");
    const a_sel = findElement("#a_sel").value;
    const a_in = a_in_el.value * a_sel;
    const b_sel = findElement("#b_sel").value;
    const b_in = b_in_el.value * b_sel;
    const hb_sel = findElement("#hb_sel").value;
    const hb_in = hb_in_el.value * hb_sel;
    const ha_sel = findElement("#ha_sel").value;
    const ha_in = ha_in_el.value * ha_sel;
    const beta_sel = findElement("#beta_sel").value;
    const beta_in = beta_in_el.value * beta_sel;
    const alpha_sel = findElement("#alpha_sel").value;
    const alpha_in = alpha_in_el.value * alpha_sel;
    const area_sel = findElement("#area_sel").value;
    const area_in = area_in_el.value * area_sel;
    const perimeter_sel = findElement("#perimeter_sel").value;
    const perimeter_in = perimeter_in_el.value * perimeter_sel;
    let a = null;
    let b = null;
    if (a_in != 0 && b_in != 0) {
        a = a_in;
        b = b_in.value;
    } else if (a_in != 0 && ha_in != 0) {
        a = a_in;
        const ha = ha_in;
        b = Math.sqrt(4 * (a ** 2 - ha ** 2));
    } else if (b_in != 0 && hb_in != 0) {
        b = b_in;
        const hb = hb_in;
        a = Math.sqrt((b / 2) ** 2 + hb ** 2);
    } else if (a_in != 0 && (alpha_in != 0 || beta_in != 0)) {
        a = a_in != 0;
        let angle;
        if (alpha_in != 0) {
            angle = alpha_in;
        } else {
            angle = (2 * Math.PI - beta_in) / 2;
        }
        b = Math.cos(angle) * a * 2;
    } else if (b_in != 0 && (alpha_in != 0 || beta_in != 0)) {
        b = b_in != 0;
        let angle;
        if (alpha_in != 0) {
            angle = alpha_in;
        } else {
            angle = (2 * Math.PI - beta_in) / 2;
        }
        a = b / (2 * Math.cos(angle));
    } else if (area_in != 0 && ha_in != 0) {
        a = 2 * area_in / ha_in;
        b = Math.sqrt(4 * (a ** 2 - ha_in ** 2));
    } else if (area_in != 0 && hb_in != 0) {
        b = 2 * area_in / hb_in;
        a = Math.sqrt((b / 2) ** 2 + hb_in ** 2);
    } else if (area_in != 0 && (alpha_in != 0 || beta_in != 0)) {
        let angle;
        if (alpha_in != 0) {
            angle = alpha_in;
        } else {
            angle = (2 * Math.PI - beta_in) / 2;
        }
        b = Math.sqrt(area_in * 6 / Math.sqrt(3));
        a = b / (2 * Math.cos(angle));
    } else if (perimeter_in != (alpha_in != 0 || beta_in != 0)) {
        let angle;
        if (alpha_in != 0) {
            angle = alpha_in;
        } else {
            angle = (2 * Math.PI - beta_in) / 2;
        }
        a = perimeter_in / (2 + Math.cos(angle));
        b = Math.cos(angle) * a * 2;
    }
    a_in_el.value = a;
    b_in_el.value = b;
    area_in_el.value = (1 / 4) * b * Math.sqrt(4 * (a ** 2) - (b ** 2));
    hb_in_el.value = (area_in.value * 2) / b;
    hb_in_el.value = (area_in.value * 2) / a;
    alpha_in_el.value = Math.acos(b / (2 * a));
    beta_in_el.value = 2 * Math.asin(b / (2 * a));
    perimeter_in_el.value = a + a + b;
}

function heron_triangle_calculator() {
    let a_in_el = findElement("#a_in");
    let b_in_el = findElement("#b_in");
    let c_in_el = findElement("#c_in");
    let area_in_el = findElement("#area_in");
    let perimeter_in_el = findElement("#perimeter_in");
    const a_sel = findElement("#a_sel").value;
    const a_in = a_in_el.value * a_sel;
    const b_sel = findElement("#b_sel").value;
    const b_in = b_in_el.value * b_sel;
    const c_sel = findElement("#c_sel").value;
    const c_in = c_in_el.value * c_sel;
    const area_sel = findElement("#area_sel").value;
    const area_in = area_in_el.value * area_sel;
    const perimeter_sel = findElement("#perimeter_sel").value;
    const perimeter_in = perimeter_in_el.value * perimeter_sel;
    let a, b, c;
    if (a_in != 0 && b_in != 0 && c_in != 0) {
        a = parseFloat(a_in);
        b = parseFloat(b_in);
        c = parseFloat(c_in);
    } else if (perimeter_in != 0) {
        if (a_in != 0 && b_in != 0) {
            a = parseFloat(a_in);
            b = parseFloat(b_in);
            c = perimeter_in - a - b;
        } else if (a_in != 0 && c_in != 0) {
            a = parseFloat(a_in);
            c = parseFloat(c_in);
            b = perimeter_in - a - c;
        } else if (b_in != 0 && c_in != 0) {
            b = parseFloat(b_in);
            c = parseFloat(c_in);
            a = perimeter_in - b - c;
        }
    } else if (area_in != 0) {
        if (a_in != 0 && b_in != 0) {
            a = parseFloat(a_in);
            b = parseFloat(b_in);
            const area = area_in;
            c = Math.sqrt((b ** 2) + (a ** 2) + ((-2) * b * a) * Math.cos(Math.asin((area * 2) / (b * a))));
            c_in_el.value = c;
            perimeter_in_el.value = a + b + c;
        } else if (a_in != 0 && c_in != 0) {
            a = parseFloat(a_in);
            c = parseFloat(c_in);
            const area = area_in;
            b = Math.sqrt((c ** 2) + (a ** 2) + ((-2) * c * a) * Math.cos(Math.asin((area * 2) / (c * a))));
            b_in_el.value = b;
            perimeter_in_el.value = a + b + c;
        } else if (b_in != 0 && c_in != 0) {
            b = parseFloat(b_in);
            c = parseFloat(c_in);
            const area = area_in;
            a = Math.sqrt((b ** 2) + (c ** 2) + ((-2) * b * c) * Math.cos(Math.asin((area * 2) / (b * c))));
            a_in_el.value = a;
            perimeter_in_el.value = a + b + c;
        }
        return;
    }
    const p = (parseFloat(a) + parseFloat(b) + parseFloat(c)) / 2;
    area_in_el.value = Math.sqrt(p * (p - a) * (p - b) * (p - c));
    perimeter_in_el.value = p * 2;
}

function cross_product_calculator() {
    let a_x_in = findElement("#a_x_in");
    let a_y_in = findElement("#a_y_in");
    let a_z_in = findElement("#a_z_in");
    let b_x_in = findElement("#b_x_in");
    let b_y_in = findElement("#b_y_in");
    let b_z_in = findElement("#b_z_in");
    let c_x_in = findElement("#c_x_in");
    let c_y_in = findElement("#c_y_in");
    let c_z_in = findElement("#c_z_in");

    if (a_x_in.value != 0 && a_y_in.value != 0 && a_z_in.value != 0 && b_x_in.value != 0 && b_y_in.value != 0 && b_z_in.value != 0) {
        const a_x = a_x_in.value;
        const a_y = a_y_in.value;
        const a_z = a_z_in.value;
        const b_x = b_x_in.value;
        const b_y = b_y_in.value;
        const b_z = b_z_in.value;
        c_x_in.value = a_y * b_z - a_z * b_y;
        c_y_in.value = a_z * b_x - a_x * b_z;
        c_z_in.value = a_x * b_y - a_y * b_x;
    }
}

function dot_product_calculator() {
    let a_x_in = findElement("#a_x_in");
    let a_y_in = findElement("#a_y_in");
    let a_z_in = findElement("#a_z_in");
    let b_x_in = findElement("#b_x_in");
    let b_y_in = findElement("#b_y_in");
    let b_z_in = findElement("#b_z_in");
    let c_in = findElement("#c_in");

    if (a_x_in.value != 0 && a_y_in.value != 0 && a_z_in.value != 0 && b_x_in.value != 0 && b_y_in.value != 0 && b_z_in.value != 0) {
        const a_x = a_x_in.value;
        const a_y = a_y_in.value;
        const a_z = a_z_in.value;
        const b_x = b_x_in.value;
        const b_y = b_y_in.value;
        const b_z = b_z_in.value;
        c_in.value = a_x * b_x + a_y * b_y + a_z * b_z;
    }
}

function perpendicular_line_calculator() {
    let fist_eq_m_in = findElement("#fist_eq_m_in");
    let first_eq_r_in = findElement("#first_eq_r_in");
    let sec_px_in = findElement("#sec_px_in");
    let sec_py_in = findElement("#sec_py_in");
    let sec_eq_a_in = findElement("#sec_eq_a_in");
    let sec_eq_b_in = findElement("#sec_eq_b_in");
    let inter_px_in = findElement("#inter_px_in");
    let inter_py_in = findElement("#inter_py_in");
    let fist_eq_m = findElement("#fist_eq_m_in");
    let first_eq_r = findElement("#first_eq_r_in");
    let sec_px = findElement("#sec_px_in").value;
    let sec_py = findElement("#sec_py_in").value;
    let sec_eq_a = findElement("#sec_eq_a_in").value;
    let sec_eq_b = findElement("#sec_eq_b_in").value;
    let inter_px = findElement("#inter_px_in").value;
    let inter_py = findElement("#inter_py_in").value;

    if (fist_eq_m != 0 && first_eq_r != 0) {
        if (sec_px != 0 && sec_py != 0) {
            sec_eq_a_in.value = -1 / fist_eq_m_in.value;
            sec_eq_b_in.value = sec_py_in.value - sec_eq_a_in.value * sec_px_in.value;
            inter_px_in.value = -(first_eq_r_in.value - sec_eq_b_in.value) / (fist_eq_m_in.value - sec_eq_a_in.value);
            inter_py_in.value = inter_px_in.value * fist_eq_m_in.value + first_eq_r_in.value;
        } else if (sec_eq_a != 0 && sec_eq_b != 0) {
            inter_px_in.value = -(first_eq_r_in.value - sec_eq_b_in.value) / (fist_eq_m_in.value - sec_eq_a_in.value);
            inter_py_in.value = inter_px_in.value * fist_eq_m_in.value + first_eq_r_in.value;
        } else if (inter_px != 0 && inter_py != 0) {
            sec_eq_a_in.value = -1 / fist_eq_m_in.value;
            sec_eq_b_in.value = inter_py_in.value - sec_eq_a_in.value * inter_px_in.value;
        }
    }
}

function cylindrical_calculator() {
    let a_x_in = findElement("#a_x_in");
    let a_y_in = findElement("#a_y_in");
    let a_z_in = findElement("#a_z_in");
    let b_x_in = findElement("#b_x_in");
    let b_y_in = findElement("#b_y_in");
    let b_z_in = findElement("#b_z_in");

    if (a_x_in.value != 0 && a_y_in.value != 0 && a_z_in.value != 0) {
        const a_x = a_x_in.value;
        const a_y = a_y_in.value;
        const a_z = a_z_in.value;
        b_x_in.value = Math.sqrt((a_x ** 2) + (a_y ** 2));
        b_y_in.value = 360 * Math.atan(a_y / a_x) / (Math.PI * 2);
        b_z_in.value = a_z;
    } else if (b_x_in.value != 0 && b_y_in.value != 0 && b_z_in.value != 0) {
        const b_x = b_x_in.value;
        const b_y = b_y_in.value;
        const b_z = b_z_in.value;
        a_x_in.value = Math.cos(b_y) * b_x;
        a_y_in.value = Math.sin(b_y) * b_x;
        a_z_in.value = b_z;
    }
}

function shoe_size_calculator() {
    let l_in = findElement("#l_in");
    let usw_in = findElement("#usw_in");
    let usm_in = findElement("#usm_in");
    let au_in = findElement("#au_in");
    let eu_in = findElement("#eu_in");
    let jp_in = findElement("#jp_in");
    let kr_in = findElement("#kr_in");

    let inches;
    if (l_in.value != 0) {
        inches = l_in.value / 25.4;
    } else if (usw_in.value != 0) {
        inches = (usw_in.value + 22) / 3;
    } else if (usm_in.value != 0) {
        inches = (usm_in.value + 21) / 3;
    } else if (au_in.value != 0) {
        inches = (au_in.value + 23) / 3;
    } else if (eu_in.value != 0) {
        inches = ((eu_in.value - 2) / 1.27 + 23) / 3;
    } else if (jp_in.value != 0) {
        inches = jp_in * 10 / 25.4;
    } else if (kr_in.value != 0) {
        inches = kr_in / 25.4;
    }
    usw_in.innerHTML = inches * 3 - 21;
    usm_in.innerHTML = inches * 3 - 22;
    au_in.innerHTML = inches * 3 - 23;
    eu_in.innerHTML = (inches * 3 * 1.27) + 2;
    jp_in.innerHTML = inches * 25.4 / 10;
    kr_in.innerHTML = inches * 25.4;
    l_in.innerHTML = inches * 25.4;
}

function triangle_calculator() {
    let sa = document.getElementById("saInput").value;
    let sb = document.getElementById("sbInput").value;
    let sc = document.getElementById("scInput").value;
    let aa = document.getElementById("aaInput").value;
    let ab = document.getElementById("abInput").value;
    let ac = document.getElementById("acInput").value;
    let aasel = document.getElementById("aaTypeSelector").value;
    let absel = document.getElementById("abTypeSelector").value;
    let acsel = document.getElementById("acTypeSelector").value;
    aasel /= 0.05625;
    absel /= 0.05625;
    acsel /= 0.05625;
    var flag = 0;
    var extraflag = 0; //whether the same item
    if (isNum(sa) && document.getElementById("saInput").style.backgroundColor != '') {
        flag++;
        sa = sa * 1;
        if (sa <= 0) {
            alert("Side value invalid.");
            document.getElementById("saInput").value = "";
            return;
        }
    }
    if (isNum(sb) && document.getElementById("sbInput").style.backgroundColor != '') {
        flag++;
        sb = sb * 1;
        if (sb <= 0) {
            alert("Side value invalid.");
            document.getElementById("sbInput").value = "";
            return;
        }
    }
    if (isNum(sc) && document.getElementById("scInput").style.backgroundColor != '') {
        flag++;
        sc = sc * 1;
        if (sc <= 0) {
            alert("Side value invalid.");
            document.getElementById("scInput").value = "";
            return;
        }
    }
    if (isNum(aa) && document.getElementById("aaInput").style.backgroundColor != '') {
        flag++;
        aa = aa * 1;
        if (aa * aasel > 3.15) {
            alert("Angle value A invalid.");
            document.getElementById("aaInput").value = "";
            document.getElementById("aaInput").style.backgroundColor = '';
            return;
        }
    }
    if (isNum(ab) && document.getElementById("abInput").style.backgroundColor != '') {

        flag++;
        ab = ab * 1;
        if (ab * absel > 3.15) {
            alert("Angle value B invalid.");
            document.getElementById("abInput").value = "";
            document.getElementById("abInput").style.backgroundColor = '';
            return;
        }
    }
    if (isNum(ac) && document.getElementById("acInput").style.backgroundColor != '') {
        flag++;
        ac = ac * 1;
        if (ac * acsel > 3.15) {
            alert("Angle value C invalid.");
            document.getElementById("acInput").value = "";
            document.getElementById("acInput").style.backgroundColor = '';
            return;
        }
    }

    if (flag < 3) {
        if (document.getElementById("saInput").style.backgroundColor == '') {
            document.getElementById("saInput").value = "";
        }
        if (document.getElementById("sbInput").style.backgroundColor == '') {
            document.getElementById("sbInput").value = "";
        }
        if (document.getElementById("scInput").style.backgroundColor == '') {
            document.getElementById("scInput").value = "";
        }
        if (document.getElementById("aaInput").style.backgroundColor == '') {
            document.getElementById("aaInput").value = "";
        }
        if (document.getElementById("abInput").style.backgroundColor == '') {
            document.getElementById("abInput").value = "";
        }
        if (document.getElementById("acInput").style.backgroundColor == '') {
            document.getElementById("acInput").value = "";
        }
        return;
    }
    //if (extraflag == 1) {alert("same");return;}

    if ((document.getElementById("aaInput").style.backgroundColor != '' && document.getElementById("abInput").style.backgroundColor != '')
        || (document.getElementById("aaInput").style.backgroundColor != '' && document.getElementById("acInput").style.backgroundColor != '')
        || (document.getElementById("abInput").style.backgroundColor != '' && document.getElementById("acInput").style.backgroundColor != '')
    ) {
        //alert("1");
        if (document.getElementById("aaInput").style.backgroundColor != '') {
            if (document.getElementById("abInput").style.backgroundColor != '') {
                ac = (180 - aa * document.getElementById("aaTypeSelector").value - ab * document.getElementById("abTypeSelector").value) / document.getElementById("acTypeSelector").value;
                //alert(ac);
            } else if (document.getElementById("acInput").style.backgroundColor != '') {
                ab = (180 - aa * document.getElementById("aaTypeSelector").value - ac * document.getElementById("acTypeSelector").value) / document.getElementById("abTypeSelector").value;
                //alert(ab);
            }
        } else if (document.getElementById("abInput").style.backgroundColor != '' && document.getElementById("acInput").style.backgroundColor != '') {
            aa = (180 - ac * document.getElementById("acTypeSelector") - ab * document.getElementById("abTypeSelector").value) / document.getElementById("aaTypeSelector").value;
            //alert(aa);
        }

        if ((document.getElementById("saInput").style.backgroundColor != '')) {
            sb = sa * Math.sin(ab * absel) / Math.sin(aa * aasel);
            sc = sa * Math.sin(ac * acsel) / Math.sin(aa * aasel);
            document.getElementById("abInput").style.backgroundColor = '';
            document.getElementById("scInput").style.backgroundColor = '';
            document.getElementById("abInput").value = "";
            document.getElementById("scInput").value = "";
        } else if (document.getElementById("sbInput").style.backgroundColor != '') {
            sa = sb * Math.sin(aa * aasel) / Math.sin(ab * absel);
            sc = sb * Math.sin(ac * acsel) / Math.sin(ab * absel);
            document.getElementById("saInput").style.backgroundColor = '';
            document.getElementById("scInput").style.backgroundColor = '';
            document.getElementById("saInput").value = "";
            document.getElementById("scInput").value = "";
        } else if (document.getElementById("scInput").style.backgroundColor != '') {
            sb = sc * Math.sin(ab * absel) / Math.sin(ac * acsel);
            sa = sc * Math.sin(aa * aasel) / Math.sin(ac * acsel);
            document.getElementById("abInput").style.backgroundColor = '';
            document.getElementById("saInput").style.backgroundColor = '';
            document.getElementById("abInput").value = "";
            document.getElementById("saInput").value = "";
        }
    } else if ((document.getElementById("saInput").style.backgroundColor != '' && document.getElementById("sbInput").style.backgroundColor != '')
        || (document.getElementById("saInput").style.backgroundColor != '' && document.getElementById("scInput").style.backgroundColor != '')
        || (document.getElementById("sbInput") != '' && document.getElementById("scInput").style.backgroundColor != '')
    ) {
        //alert("2");
        if (document.getElementById("saInput").style.backgroundColor != '') {
            if (document.getElementById("sbInput").style.backgroundColor != '') {
                if (document.getElementById("aaInput").style.backgroundColor != '') {
                    //*
                    var b = -2 * sb * Math.cos(aa * aasel);
                    var c = sb * sb - sa * sa;

                    var x1 = (-1 * b + Math.sqrt(b * b - 4 * c)) / (2);
                    var x2 = (-1 * b - Math.sqrt(b * b - 4 * c)) / (2);

                    if (Math.abs((sb * sb + x1 * x1 - sa * sa) / (2 * sb * x1)) > 1
                        && Math.abs((sb * sb + x2 * x2 - sa * sa) / (2 * sb * x2)) > 1) {
                        alert("Can't get a triangle, please check your data!")
                        return;
                    } else if (Math.abs((sb * sb + x1 * x1 - sa * sa) / (2 * sb * x1)) > 1
                        && Math.abs((sb * sb + x2 * x2 - sa * sa) / (2 * sb * x2)) <= 1)
                        sc = x2;
                    else if (Math.abs((sb * sb + x1 * x1 - sa * sa) / (2 * sb * x1)) <= 1
                        && Math.abs((sb * sb + x2 * x2 - sa * sa) / (2 * sb * x2)) > 1)
                        sc = x1;
                    else if (Math.abs((sb * sb + x1 * x1 - sa * sa) / (2 * sb * x1)) <= 1
                        && Math.abs((sb * sb + x2 * x2 - sa * sa) / (2 * sb * x2)) <= 1) {
                        if (document.getElementById("scInput").value == x1 || document.getElementById("scInput").value == x2) return;
                        if (x1 <= 0) {
                            sc = x2;
                        } else if (x2 <= 0) {
                            sc = x1;
                        } else if (confirm("Side C has two value. Is " + x1 + " correct? Or select cancel to select " + x2 + ".")) {
                            sc = x1;
                        } else
                            sc = x2;
                    }
                    //*/
                    //ab = Math.asin(sb * Math.sin(aa*aasel)/sa)/absel;
                    //alert("ab is: " + ab);
                } else if (document.getElementById("abInput").style.backgroundColor != '') {
                    var b = -2 * sa * Math.cos(ab * absel);
                    var c = sa * sa - sb * sb;

                    var x1 = (-1 * b + Math.sqrt(b * b - 4 * 1 * c)) / (2 * 1);
                    var x2 = (-1 * b - Math.sqrt(b * b - 4 * 1 * c)) / (2 * 1);

                    if (Math.abs((sa * sa + x1 * x1 - sb * sb) / (2 * sa * x1)) > 1
                        && Math.abs((sa * sa + x2 * x2 - sb * sb) / (2 * sa * x2)) > 1)
                        return;
                    else if (Math.abs((sa * sa + x1 * x1 - sb * sb) / (2 * sa * x1)) > 1
                        && Math.abs((sa * sa + x2 * x2 - sb * sb) / (2 * sa * x2)) <= 1)
                        sc = x2;
                    else if (Math.abs((sa * sa + x1 * x1 - sb * sb) / (2 * sa * x1)) <= 1
                        && Math.abs((sa * sa + x2 * x2 - sb * sb) / (2 * sa * x2)) > 1)
                        sc = x1;
                    else if (Math.abs((sa * sa + x1 * x1 - sb * sb) / (2 * sa * x1)) <= 1
                        && Math.abs((sa * sa + x2 * x2 - sb * sb) / (2 * sa * x2)) <= 1) {
                        if (document.getElementById("scInput").value == x1 || document.getElementById("scInput").value == x2) return;
                        if (confirm("Side C has two value. Is " + x1 + " correct? Or select cancel to select " + x2 + ".")) {
                            sc = x1;
                        } else
                            sc = x2;
                    }
                } else if (document.getElementById("acInput").style.backgroundColor != '') {
                    sc = Math.sqrt(sb * sb + sa * sa - 2 * sb * sa * Math.cos(ac * acsel));
                } else if (document.getElementById("scInput").style.backgroundColor != '') {
                    if (sa + sb <= sc || sa + sc <= sb || sb + sc <= sa) {
                        //alert("3 sides");
                        /*                        document.triangle.area.value = "";
                                                document.triangle.perimeter.value = "";
                                                document.triangle.ccr.value = "";
                                                document.triangle.icr.value = "";
                                                document.triangle.ha.value = "";
                                                document.triangle.hb.value = "";
                                                document.triangle.hc.value = "";
                                                document.triangle.bisa.value = "";
                                                document.triangle.bisb.value = "";
                                                document.triangle.bisc.value = "";
                                                document.triangle.meda.value = "";
                                                document.triangle.medb.value = "";
                                                document.triangle.medc.value = "";
                                                document.triangle.ab.value = "";
                                                document.triangle.aa.value = "";
                                                document.triangle.ac.value = "";*/
                        return;
                    } else {
                        aa = Math.acos((sb * sb + sc * sc - sa * sa) / (2 * sb * sc)) * 180 / (Math.PI * document.getElementById("aaTypeSelector").value);
                        ab = Math.acos((sa * sa + sc * sc - sb * sb) / (2 * sa * sc)) * 180 / (Math.PI * document.getElementById("abTypeSelector").value);
                        ac = Math.acos((sb * sb + sa * sa - sc * sc) / (2 * sb * sa)) * 180 / (Math.PI * document.getElementById("acTypeSelector").value);
                    }
                }
            } else if (document.getElementById("scInput").style.backgroundColor != '') {
                if (document.getElementById("aaInput").style.backgroundColor != '') {
                    var b = -2 * sc * Math.cos(aa * aasel);
                    var c = sc * sc - sa * sa;

                    var x1 = (-1 * b + Math.sqrt(b * b - 4 * 1 * c)) / (2 * 1);
                    var x2 = (-1 * b - Math.sqrt(b * b - 4 * 1 * c)) / (2 * 1);

                    if (Math.abs((sc * sc + x1 * x1 - sa * sa) / (2 * sc * x1)) > 1
                        && Math.abs((sc * sc + x2 * x2 - sa * sa) / (2 * sc * x2)) > 1)
                        return;
                    else if (Math.abs((sc * sc + x1 * x1 - sa * sa) / (2 * sc * x1)) > 1
                        && Math.abs((sc * sc + x2 * x2 - sa * sa) / (2 * sc * x2)) <= 1)
                        sb = x2;
                    else if (Math.abs((sc * sc + x1 * x1 - sa * sa) / (2 * sc * x1)) <= 1
                        && Math.abs((sc * sc + x2 * x2 - sa * sa) / (2 * sc * x2)) > 1)
                        sb = x1;
                    else if (Math.abs((sc * sc + x1 * x1 - sa * sa) / (2 * sc * x1)) <= 1
                        && Math.abs((sc * sc + x2 * x2 - sa * sa) / (2 * sc * x2)) <= 1) {
                        if (document.getElementById("abInput").value == x1 || document.getElementById("abInput").value == x2) return;
                        if (x1 <= 0) {
                            sc = x2;
                        } else if (x2 <= 0) {
                            sc = x1;
                        } else if (confirm("Side C has two value. Is " + x1 + " correct? Or select cancel to select " + x2 + ".")) {
                            sb = x1;
                        } else
                            sb = x2;
                    }
                } else if (document.getElementById("acInput").style.backgroundColor != '') {
                    var b = -2 * sa * Math.cos(ab * absel);
                    var c = sa * sa - sc * sc;

                    var x1 = (-1 * b + Math.sqrt(b * b - 4 * 1 * c)) / (2 * 1);
                    var x2 = (-1 * b - Math.sqrt(b * b - 4 * 1 * c)) / (2 * 1);

                    if (Math.abs((sa * sa + x1 * x1 - sc * sc) / (2 * sa * x1)) > 1
                        && Math.abs((sa * sa + x2 * x2 - sc * sc) / (2 * sa * x2)) > 1)
                        return;
                    else if (Math.abs((sa * sa + x1 * x1 - sc * sc) / (2 * sa * x1)) > 1
                        && Math.abs((sa * sa + x2 * x2 - sc * sc) / (2 * sa * x2)) <= 1)
                        sb = x2;
                    else if (Math.abs((sa * sa + x1 * x1 - sc * sc) / (2 * sa * x1)) <= 1
                        && Math.abs((sa * sa + x2 * x2 - sc * sc) / (2 * sa * x2)) > 1)
                        sb = x1;
                    else if (Math.abs((sa * sa + x1 * x1 - sc * sc) / (2 * sa * x1)) <= 1
                        && Math.abs((sa * sa + x2 * x2 - sc * sc) / (2 * sa * x2)) <= 1) {
                        if (document.getElementById("sbInput").value == x1 || document.getElementById("sbInput").value == x2) return;
                        if (x1 <= 0) {
                            sc = x2;
                        } else if (x2 <= 0) {
                            sc = x1;
                        } else if (confirm("Side C has two value. Is " + x1 + " correct? Or select cancel to select " + x2 + ".")) {
                            sb = x1;
                        } else
                            sb = x2;
                    }
                } else if (document.getElementById("abInput").style.backgroundColor != '') {
                    sb = Math.sqrt(sc * sc + sa * sa - 2 * sc * sa * Math.cos(ab * absel));
                }
            }
        } else if (document.getElementById("sbInput").style.backgroundColor != '' && document.getElementById("scInput").style.backgroundColor != '') {
            if (document.getElementById("abInput").style.backgroundColor != '') {
                var b = -1 * 2 * sc * Math.cos(ab * absel);
                var c = sc * sc - sb * sb;

                var x1 = (-1 * b + Math.sqrt(b * b - 4 * 1 * c)) / (2 * 1);
                var x2 = (-1 * b - Math.sqrt(b * b - 4 * 1 * c)) / (2 * 1);

                if (Math.abs((sc * sc + x1 * x1 - sb * sb) / (2 * sc * x1)) > 1
                    && Math.abs((sc * sc + x2 * x2 - sb * sb) / (2 * sc * x2)) > 1)
                    return;
                else if (Math.abs((sc * sc + x1 * x1 - sb * sb) / (2 * sc * x1)) > 1
                    && Math.abs((sc * sc + x2 * x2 - sb * sb) / (2 * sc * x2)) <= 1)
                    sa = x2;
                else if (Math.abs((sc * sc + x1 * x1 - sb * sb) / (2 * sc * x1)) <= 1
                    && Math.abs((sc * sc + x2 * x2 - sb * sb) / (2 * sc * x2)) > 1)
                    sa = x1;
                else if (Math.abs((sc * sc + x1 * x1 - sb * sb) / (2 * sc * x1)) <= 1
                    && Math.abs((sc * sc + x2 * x2 - sb * sb) / (2 * sc * x2)) <= 1) {
                    if (document.getElementById("saInput").value == x1 || document.getElementById("saInput").value == x2) return;
                    if (x1 <= 0) {
                        sc = x2;
                    } else if (x2 <= 0) {
                        sc = x1;
                    } else if (confirm("Side C has two value. Is " + x1 + " correct? Or select cancel to select " + x2 + ".")) {
                        sa = x1;
                    } else
                        sa = x2;
                }
            } else if (document.getElementById("acInput").style.backgroundColor != '') {
                var b = -1 * 2 * sb * Math.cos(ac * acsel);
                var c = sb * sb - sc * sc;

                var x1 = (-1 * b + Math.sqrt(b * b - 4 * 1 * c)) / (2 * 1);
                var x2 = (-1 * b - Math.sqrt(b * b - 4 * 1 * c)) / (2 * 1);

                if (Math.abs((sb * sb + x1 * x1 - sc * sc) / (2 * sb * x1)) > 1
                    && Math.abs((sb * sb + x2 * x2 - sc * sc) / (2 * sb * x2)) > 1)
                    return;
                else if (Math.abs((sb * sb + x1 * x1 - sc * sc) / (2 * sb * x1)) > 1
                    && Math.abs((sb * sb + x2 * x2 - sc * sc) / (2 * sb * x2)) <= 1)
                    sa = x2;
                else if (Math.abs((sb * sb + x1 * x1 - sc * sc) / (2 * sb * x1)) <= 1
                    && Math.abs((sb * sb + x2 * x2 - sc * sc) / (2 * sb * x2)) > 1)
                    sa = x1;
                else if (Math.abs((sb * sb + x1 * x1 - sc * sc) / (2 * sb * x1)) <= 1
                    && Math.abs((sb * sb + x2 * x2 - sc * sc) / (2 * sb * x2)) <= 1) {
                    if (document.getElementById("saInput").value == x1 || document.getElementById("saInput").value == x2) return;
                    if (x1 <= 0) {
                        sc = x2;
                    } else if (x2 <= 0) {
                        sc = x1;
                    } else if (confirm("Side C has two value. Is " + x1 + " correct? Or select cancel to select " + x2 + ".")) {
                        sa = x1;
                    } else
                        sa = x2;
                }
            } else if (document.getElementById("aaInput").style.backgroundColor != '') {
                sa = Math.sqrt(sc * sc + sb * sb - 2 * sc * sb * Math.cos(aa * aasel));
            }
        }

        if (document.getElementById("aaInput").style.backgroundColor == '')
            aa = Math.acos((sb * sb + sc * sc - sa * sa) / (2 * sb * sc)) / aasel;
        if (document.getElementById("abInput").style.backgroundColor == '') {
            ab = Math.acos((sa * sa + sc * sc - sb * sb) / (2 * sa * sc)) / absel;
        }
        if (document.getElementById("acInput").style.backgroundColor == '') {
            ac = Math.acos((sa * sa + sb * sb - sc * sc) / (2 * sa * sb)) / acsel;
        }
    }
    var area = sa * sb * 0.5 * Math.sin(ac * acsel);
    var perimeter = sa + sb + sc;
    var ccr = sa * 0.5 / Math.sin(aa * aasel);
    var icr = sc * Math.sin(aa * aasel / 2) * Math.sin(ab * absel / 2) / Math.cos(ac * acsel / 2);
    var ha = 2 * area / sa;
    var hb = 2 * area / sb;
    var hc = 2 * area / sc;
    var ta = 2 * sb * sc * Math.cos(aa * aasel / 2) / (sb + sc);
    var tb = 2 * sa * sc * Math.cos(ab * absel / 2) / (sb + sc);
    var tc = 2 * sa * sb * Math.cos(ac * acsel / 2) / (sb + sc);
    var meda = Math.sqrt(sb * sb + sc * sc - sa * sa * 0.5);
    var medb = Math.sqrt(sa * sa + sc * sc - sb * sb * 0.5);
    var medc = Math.sqrt(sb * sb + sa * sa - sc * sc * 0.5);
    let res_div = document.getElementById("result");
    res_div.removeChild(res_div.firstChild);
    let res_lbl = document.createElement("P");
    res_lbl.innerHTML = area + "<br>" + perimeter + "<br>" + ha + "<br>" + hb + "<br>" + hc + "<br>" + ta + "<br>" + tb + "<br>" + tc + "<br>" + meda + "<br>" + medb + "<br>" + medc + "<br>" + icr + "<br>" + ccr;
    document.getElementById("result").appendChild(res_lbl);
}

function qrcode_generator() {
    /*    const script = document.createElement("script");
        script.src = "https://cdnjs.cloudflare.com/ajax/libs/qrious/4.0.2/qrious.min.js";
        head.appendChild(script);*/
    const url = findElement("#linkInput");
    let img = findElement("#qrcode_img");
    let qr = new QRious({
        background: 'white',
        backgroundAlpha: 0.8,
        foreground: 'black',
        foregroundAlpha: 0.8,
        level: 'H',
        padding: 25,
        size: 500,
        value: String("https://" + url.value),
    })
    img.setAttribute('src', qr.image.src);
}

function chmod_calculator() {
    var read_user = findElement('input[name="read_user_radio"]:checked').value;
    var read_group = findElement('input[name="read_group_radio"]:checked').value;
    var read_other = findElement('input[name="read_other_radio"]:checked').value;
    var write_user = findElement('input[name="write_user_radio"]:checked').value;
    var write_group = findElement('input[name="write_group_radio"]:checked').value;
    var write_other = findElement('input[name="write_other_radio"]:checked').value;
    var execute_user = findElement('input[name="execute_user_radio"]:checked').value;
    var execute_group = findElement('input[name="execute_group_radio"]:checked').value;
    var execute_other = findElement('input[name="execute_other_radio"]:checked').value;
    let binary = '';
    if (read_user == 1) {
        binary += '1';
    } else {
        binary += '0';
    }
    if (write_user == 1) {
        binary += '1';
    } else {
        binary += '0';
    }
    if (execute_user == 1) {
        binary += '1';
    } else {
        binary += '0';
    }
    if (read_group == 1) {
        binary += '1';
    } else {
        binary += '0';
    }
    if (write_group == 1) {
        binary += '1';
    } else {
        binary += '0';
    }
    if (execute_group == 1) {
        binary += '1';
    } else {
        binary += '0';
    }
    if (read_other == 1) {
        binary += '1';
    } else {
        binary += '0';
    }
    if (write_other == 1) {
        binary += '1';
    } else {
        binary += '0';
    }
    if (execute_other == 1) {
        binary += '1';
    } else {
        binary += '0';
    }
    let symbolic = '';
    for (let i = 0; i < 9; i++) {
        if ((binary.charAt(i) === '1') && (i % 3 === 0)) symbolic += 'r';
        else if ((binary.charAt(i) === '1') && (i % 3 === 1)) symbolic += 'w';
        else if ((binary.charAt(i) === '1') && (i % 3 === 2)) symbolic += 'x';
        else symbolic += '-';
    }
    let octal = '';
    octal += (parseInt(binary.slice(0, 3), 2).toString(8));
    octal += (parseInt(binary.slice(3, 6), 2).toString(8));
    octal += (parseInt(binary.slice(6, 9), 2).toString(8));
    var codeDiv = document.createElement("div");
    codeDiv.innerHTML = "<b>chmod " + octal + "</b> file_name";
    var symbolicDiv = document.createElement("div");
    symbolicDiv.innerHTML = "Symbolic: " + symbolic;
    var binaryDiv = document.createElement("div");
    binaryDiv.innerHTML = "Binary: " + binary;
    var octalDiv = document.createElement("div");
    octalDiv.innerHTML = "Octal: " + octal;
    if (curr_json.areas.output && !findElement('#' + result_id)) {
        const resultEl = curr_json.areas.output;
        areaCreator(inputs_header = resultEl.inputs_header, id = resultEl.id, col_class = resultEl.col_class);
        findElement('#' + resultEl.id).innerHTML = '';
        findElement('#' + resultEl.id).appendChild(codeDiv);
        findElement('#' + resultEl.id).appendChild(symbolicDiv);
        findElement('#' + resultEl.id).appendChild(binaryDiv);
        findElement('#' + resultEl.id).appendChild(octalDiv);
    }
}

function image_file_size_calculator() {
    var bitDepth_in = findElement("#bitDepth_in");
    var imgHeight_in = findElement("#imgHeight_in");
    var imgWidth_in = findElement("#imgWidth_in");
    var resolution_in = findElement("#resolution_in");
    var fileSize_in = findElement("#fileSize_in");
    const bitDepth = bitDepth_in.value;
    const fileSizeValue = findElement("#fileSize_sel").value;
    let h, j;
    if (imgHeight_in.value != 0 && imgWidth_in.value != 0) {
        h = imgHeight_in.value;
        j = imgWidth_in.value;
    } else if (resolution_in.value != 0) {
        if (imgHeight_in.value != 0) {
            h = imgHeight_in.value;
            j = resolution_in.value / h;
        } else if (imgWidth_in.value != 0) {
            j = imgWidth_in.value;
            h = resolution_in.value / j;
        } else if (fileSize_in.value == 0) {
            fileSize_in.value = resolution_in.value * bitDepth * fileSizeValue;
        }
    } else {
        const fileSize = fileSize_in.value;
        if (imgHeight_in.value != 0) {
            h = imgHeight_in.value;
            j = fileSize / h;
        } else if (imgWidth_in.value != 0) {
            j = imgWidth_in.value;
            h = fileSize / j;
        } else if (resolution_in.value == 0) {
            resolution_in.value = fileSize / bitDepth / fileSizeValue;
        }
    }
    imgHeight_in.value = h;
    imgWidth_in.value = j;
    resolution_in.value = h * j;
    fileSize_in.value = resolution_in.value * bitDepth * fileSizeValue;
}

function pizza_dough_calculator() {
    const style_sel = findElement("#style_sel").value;
    const size_in = findElement("#size_in").value;
    const num_in = findElement("#num_in").value;
    const water_in = findElement("#water_in").value;
    if (style_sel == 0) {
        let flour, water, salt, yeast;
        let z = size_in / ((water_in / 100) + 0.032 + 1);
        flour = Math.round(z);
        water = Math.round((water_in * z) / 100);
        salt = Math.round(((0.03 * z) + Number.EPSILON) * 10) / 10;
        yeast = Math.round(((0.002 * z) + Number.EPSILON) * 10) / 10;
        var codeDiv = document.createElement("div");
        codeDiv.innerHTML = "Flour: " + flour + " g";
        var symbolicDiv = document.createElement("div");
        symbolicDiv.innerHTML = "Water: " + water + " ml";
        var binaryDiv = document.createElement("div");
        binaryDiv.innerHTML = "Salt: " + salt + " g";
        var octalDiv = document.createElement("div");
        octalDiv.innerHTML = "Yeast: " + yeast + " g";
        if (curr_json.areas.output && !findElement('#' + result_id)) {
            const resultEl = curr_json.areas.output;
            areaCreator(inputs_header = resultEl.inputs_header, id = resultEl.id, col_class = resultEl.col_class);
            findElement('#' + resultEl.id).innerHTML = '';
            findElement('#' + resultEl.id).appendChild(codeDiv);
            findElement('#' + resultEl.id).appendChild(symbolicDiv);
            findElement('#' + resultEl.id).appendChild(binaryDiv);
            findElement('#' + resultEl.id).appendChild(octalDiv);
        }

    } else if (style_sel == 1) {
        let flour, water, salt, yeast, oil, sugar;
        let z = size_in / ((water_in / 100) + 0.02 + 0.004 + 0.025 + 0.02 + 1);
        flour = Math.round(z);
        water = Math.round((water_in * z) / 100);
        salt = Math.round(((0.02 * z) + Number.EPSILON) * 10) / 10;
        yeast = Math.round(((0.004 * z) + Number.EPSILON) * 10) / 10;
        oil = Math.round(((0.025 * z) + Number.EPSILON) * 10) / 10;
        sugar = Math.round(((0.02 * z) + Number.EPSILON) * 10) / 10;
        var codeDiv = document.createElement("div");
        codeDiv.innerHTML = "Flour: " + flour + " g";
        var symbolicDiv = document.createElement("div");
        symbolicDiv.innerHTML = "Water: " + water + " ml";
        var binaryDiv = document.createElement("div");
        binaryDiv.innerHTML = "Salt: " + salt + " g";
        var octalDiv = document.createElement("div");
        octalDiv.innerHTML = "Yeast: " + yeast + " g";
        var oilDiv = document.createElement("div");
        oilDiv.innerHTML = "Oil: " + oil + " ml";
        var sugarDiv = document.createElement("div");
        sugarDiv.innerHTML = "Sugar: " + sugar + " g";
        if (curr_json.areas.output && !findElement('#' + result_id)) {
            const resultEl = curr_json.areas.output;
            areaCreator(inputs_header = resultEl.inputs_header, id = resultEl.id, col_class = resultEl.col_class);
            findElement('#' + resultEl.id).innerHTML = '';
            findElement('#' + resultEl.id).appendChild(codeDiv);
            findElement('#' + resultEl.id).appendChild(symbolicDiv);
            findElement('#' + resultEl.id).appendChild(binaryDiv);
            findElement('#' + resultEl.id).appendChild(octalDiv);
            findElement('#' + resultEl.id).appendChild(oilDiv);
            findElement('#' + resultEl.id).appendChild(sugarDiv);

        }
    } else {
        let flour, water, salt, yeast, oil;
        let z = size_in / ((water_in / 100) + 0.02 + 0.015 + 0.015 + 1);
        flour = Math.round(z);
        water = Math.round((water_in * z) / 100);
        salt = Math.round(((0.02 * z) + Number.EPSILON) * 10) / 10;
        yeast = Math.round(((0.004 * z) + Number.EPSILON) * 10) / 10;
        oil = Math.round(((0.025 * z) + Number.EPSILON) * 10) / 10;
        var codeDiv = document.createElement("div");
        codeDiv.innerHTML = "Flour: " + flour + " g";
        var symbolicDiv = document.createElement("div");
        symbolicDiv.innerHTML = "Water: " + water + " ml";
        var binaryDiv = document.createElement("div");
        binaryDiv.innerHTML = "Salt: " + salt + " g";
        var octalDiv = document.createElement("div");
        octalDiv.innerHTML = "Yeast: " + yeast + " g";
        var oilDiv = document.createElement("div");
        oilDiv.innerHTML = "Oil: " + oil + " ml";
        if (curr_json.areas.output && !findElement('#' + result_id)) {
            const resultEl = curr_json.areas.output;
            areaCreator(inputs_header = resultEl.inputs_header, id = resultEl.id, col_class = resultEl.col_class);
            findElement('#' + resultEl.id).innerHTML = '';
            findElement('#' + resultEl.id).appendChild(codeDiv);
            findElement('#' + resultEl.id).appendChild(symbolicDiv);
            findElement('#' + resultEl.id).appendChild(binaryDiv);
            findElement('#' + resultEl.id).appendChild(octalDiv);
            findElement('#' + resultEl.id).appendChild(oilDiv);
        }
    }


}
