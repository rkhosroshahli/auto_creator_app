function fileHanlder(files) {
    const link = "app.html?json=";
    let body = document.getElementById("cards");
    for (const f of files.inputs) {
        console.log(f);
        let div = document.createElement("DIV");
        div.className = "col-md-4";
        let linker = document.createElement("A");
        linker.setAttribute("href", link+f.href);
        let card = document.createElement("DIV");
        card.className = "card mb-4 shadow-sm text-center";
        card.style = "height: 7rem;";
        let title = document.createElement("H4");
        title.className = "card-title mt-3";
        title.innerHTML = f.title;
        card.appendChild(title);
        let p = document.createElement("P");
        p.className = "card-text";
        p.innerHTML = f.description;
        card.appendChild(p);
        linker.appendChild(card);
        div.appendChild(linker);
        body.appendChild(div);
    }
}

$(document).ready(function () {
    const jsonFile = "info.json";
    let file;

    fetch(jsonFile)
        .then(response => response.json())
        .then(json => file = json)
        .then(() => fileHanlder(file));
});


var filenames=[]
$(document).ready(function () {
    var loc = './inputs_name.json';
    /*$.get(loc, function (response) {
        console.log(response);
    })*/
    $.ajax({
        url: loc,
        success: function (data) {
            console.log(data.inputs.length);
            for (let i = 0; i < data.inputs.length; i++) {
                const tool = data.inputs[i];
                let tool_div = document.createElement("DIV");
                tool_div.className = "col-md-4";
                let link = document.createElement("A");
                link.href = "app.html?json=" + tool.file;
                inner_div = document.createElement("DIV");
                inner_div.className = "card mb-4 shadow-sm text-center";
                inner_div.stlye = "height: 7rem;";
                let header = document.createElement("H4");
                header.className = "card-title mt-3";
                header.innerHTML = tool.head;
                let p = document.createElement("P");
                p.className = "card-text";
                p.innerHTML = tool.detail;
                inner_div.appendChild(header);
                inner_div.appendChild(p);
                link.appendChild(inner_div);
                tool_div.appendChild(link);
                document.getElementById("row_div").appendChild(tool_div);
            }
        }
    });
});
